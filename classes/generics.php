<?php

class Player_Reduced {
    protected $data;

    function __construct($data) {
        $this->data = $data;
    }

    function get_data_array() {
        return $this->data;
    }
}


class Player {
    protected $name;
    protected $role;
    protected $level;
    protected $ignorePermissions;
    protected $job;
    protected $job_rank;

    function __construct($c_name, $c_role, $c_level, $c_ignorePermissions, $c_job, $c_job_rank) {
        $this->name = $c_name;
        $this->role = $c_role;
        $this->level = $c_level;
        $this->ignorePermissions = $c_ignorePermissions;
        $this->job = $c_job;
        $this->job_rank = $c_job_rank;
    }
    
    function get_name() {
        return $this->name;
    }

    function get_level() {
        return $this->level;
    }

    function get_role() {
        return $this->role;
    }

    function get_ignorePermissions() {
        return $this->ignorePermissions;
    }

    function get_job() {
        return $this->job;
    }

    function get_job_rank() {
        return $this->job_rank;
    }

    function get_data_array() {
        return [
            'name'              => $this->name, 
            'role'              => $this->role, 
            'level'             => $this->level, 
            'ignorePermissions' => $this->ignorePermissions, 
            'job'               => $this->job, 
            'job_rank'          => $this->job_rank
        ];
    }

}