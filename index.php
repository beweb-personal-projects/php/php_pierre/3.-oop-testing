<?php
require('classes/generics.php');
require('data.php');

$all_players = []; 
$all_staffs = [];

$strackz1 = new Player_Reduced($data[0]);


function createPlayers() {
    global $data;
    global $all_players;

    foreach ($data as $i => $value) {
        array_push($all_players, new Player($data[$i]['name'], $data[$i]['role'], $data[$i]['level'], $data[$i]['ignorePermissions'], $data[$i]['job'], $data[$i]['job_rank']));
    }
}

createPlayers();

echo "<pre>";
var_dump($all_players);
echo "</pre>";

echo "---------------------------------------------------------------------------------------------------------";

function getStaff() {
    global $all_players;
    global $all_staffs;

    foreach ($all_players as $i => $value) {
        if($all_players[$i]->get_role() !== "user" || $all_players[$i]->get_ignorePermissions()) {
            array_push($all_staffs, $all_players[$i]);
        }
    }
}

getStaff();

echo "<pre>";
var_dump($all_staffs);
echo "</pre>";
