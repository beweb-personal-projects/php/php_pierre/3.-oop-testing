<?php

$data = [
    [
        'name'              => 'strackz1',
        'role'              => 'admin',
        'level'             => 20,
        'ignorePermissions' => false,
        'job'               => 'police',
        'job_rank'          => 'colonel', 
    ],
    [
        'name'              => 'strackz2',
        'role'              => 'superadmin',
        'level'             => 30,
        'ignorePermissions' => true,
        'job'               => 'ambulance',
        'job_rank'          => 'director', 
    ],
    [
        'name'              => 'strackz3',
        'role'              => 'user',
        'level'             => 1,
        'ignorePermissions' => false,
        'job'               => 'mafia',
        'job_rank'          => 'capo', 
    ],
    [
        'name'              => 'strackz4',
        'role'              => 'helper',
        'level'             => 5,
        'ignorePermissions' => false,
        'job'               => 'mechanic',
        'job_rank'          => 'chief', 
    ],
    [
        'name'              => 'strackz5',
        'role'              => 'user',
        'level'             => 1,
        'ignorePermissions' => true,
        'job'               => 'police',
        'job_rank'          => 'seargent', 
    ]
];